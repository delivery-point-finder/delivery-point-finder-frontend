import axios from 'axios';

const http = axios.create({
    baseURL: process.env.REACT_APP_BASE_API_URL,
});

http.interceptors.request.use(
    config => {
        const token = getLoggedUserToken();
        config.headers["Authorization"] = `Bearer ${token}`;
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

http.interceptors.response.use((response) => {
    return response;
}, function (error) {
    if (401 === error.response.status) {
        localStorage.setItem('user-token', '');
        window.location = '/login';
        return;
    }
    return Promise.reject(error.response);
});

http.defaults.headers.common['If-Modified-Since'] = '0';
http.defaults.timeout = 180000;

export default {
    authCheck() {
        return http.get('/auth/check')
            .then(res => res.data);
    },
    login(loginObj) {
        return http.post('/auth/login', loginObj)
            .then(res => res.data);
    },
    searchByCep(cep) {
        return http.get(`/pos/from-cep/${cep}`)
            .then(res => res.data);
    },
    searchByCepList(formData) {
        const config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                responseType: 'blob'
            }
        };

        return http.post(`/pos/from-cep-list/`, formData, config)
            .then(response => response.data);
    }
}

function getLoggedUserToken() {
    return localStorage.getItem('user-token');
}