import React from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import Login from './components/Login';
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import SearchByCep from './components/SearchByCep';
import SearchByCepList from './components/SearchByCepList';
import PrivateRoute from "./components/PrivateRoute";

const styles = {
    appBar: {
        borderBottom: `1px solid rgba(0, 0, 0, 0.12)`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: '8px 12px',
    },
};

class App extends React.Component {
    render() {
        const { location } = this.props

        return (
            <main>
                <React.Fragment>
                    <CssBaseline/>
                    {location.pathname !== '/login' ?
                        <AppBar position="static" color="default" elevation={0} style={styles.appBar}>
                            <Toolbar style={styles.toolbar}>
                                <Typography variant="h6" color="inherit" noWrap style={styles.toolbarTitle}>
                                    Delivery Point Finder
                                </Typography>
                                <nav>
                                    <Link variant="button" color="textPrimary" href="/" style={styles.link}>
                                        Home
                                    </Link>
                                    <Link variant="button" color="textPrimary" href="/search-by-cep" style={styles.link}>
                                        Search by CEP
                                    </Link>
                                    <Link variant="button" color="textPrimary" href="/search-by-cep-list" style={styles.link}>
                                        Search by CEP List
                                    </Link>
                                </nav>
                            </Toolbar>
                        </AppBar>
                        : null }
                    <Switch>
                        <PrivateRoute path="/" component={Home} exact/>
                        <Route path="/login" component={Login}/>
                        <PrivateRoute path="/search-by-cep" component={SearchByCep} exact />
                        <PrivateRoute path="/search-by-cep-list" component={SearchByCepList} exact />
                        <Route component={Error}/>
                    </Switch>
                </React.Fragment>
            </main>
        );
    }

}

export default withRouter(App);
