import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import api from '../api'

export default class PrivateRoute extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false
        }
    }

    componentWillMount() {
        api.authCheck()
            .then((isAuthenticated) => {
                this.setState({isAuthenticated: isAuthenticated});
            })
            .catch((e) => {
                this.setState({isAuthenticated: false});
            })
    }

    render() {
        const {children, ...rest} = this.props;

        return (
            <Route
                {...rest}
                render={({location}) => {
                    if (!this.state.isAuthenticated) {
                        return (
                            <Redirect
                                to={{
                                    pathname: "/login",
                                    state: {from: location}
                                }}
                            />
                        );

                    }
                    return (children)
                }

                }
            />
        );
    }
}