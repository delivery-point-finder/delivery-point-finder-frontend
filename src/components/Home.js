import React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
    },
    cardPricing: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing(2),
    },
    footer: {
        borderTop: `1px solid ${theme.palette.divider}`,
        marginTop: theme.spacing(8),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        [theme.breakpoints.up('sm')]: {
            paddingTop: theme.spacing(6),
            paddingBottom: theme.spacing(6),
        },
    },
}));

const functionalities = [
    {
        title: 'Search by CEP',
        description: 'Here you can search for Delivery Point near a CEP',
        link: '/search-by-cep'
    },
    {
        title: 'Search by CEP List',
        description: 'Here you can search for Delivery Point near a CEPs list',
        link: '/search-by-cep-list'
    },
];

export default function Home() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Container maxWidth="md" component="main" className={classes.heroContent}>
                <Grid container spacing={5} alignItems="center" justify="center">
                    {functionalities.map((func) => (
                        <Grid item key={func.title} xs={12} sm={12} md={4}>
                            <Card>
                                <CardHeader
                                    title={func.title}
                                    titleTypographyProps={{ align: 'center' }}
                                    className={classes.cardHeader}
                                />
                                <CardContent>
                                    <Typography component="p" align="center">
                                        {func.description}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button href={func.link} fullWidth variant="contained" color="primary">
                                        Search
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </React.Fragment>
    );
}