import React from "react";
import api from "../api";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import LinearProgress from "@material-ui/core/LinearProgress";
import Link from "@material-ui/core/Link";

const style = {
    paper: {
        marginTop: '64px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    innerContainer: {
        width: '100%', // Fix IE 11 issue.
        marginTop: '24px',
    },
    submit: {
        margin: '24px 0 16px',
    },
    result: {
        marginTop: '24px',
    }
};

class SearchByCepList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            file: {},
            responseFile: {},
            loading: false,
            responseError: '',
        };

        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFileChange(event) {
        this.setState({
            responseError: ''
        });

        const file = event.target.files[0];
        if ( file && !/\.(csv)$/i.test( file.name ) ) {
            this.setState({
                responseError: 'Invalid file type, only CSV is accepted.'
            });
            return;
        }

        this.setState({
            file: file
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        const { file } = this.state;

        if (file) {
            this.setState({
                loading: true,
                responseError: ''
            });

            const formData = new FormData();
            formData.append('cepsList', file)

            api.searchByCepList(formData)
                .then((response) => {
                    const url = window.URL.createObjectURL(new Blob([response]));
                    const link = document.createElement('a');
                    link.style.display = "none";
                    link.href = url;
                    link.setAttribute('download', 'list-of-near-points-of-delivery-by-cep.csv');
                    document.body.appendChild(link);
                    link.click();
                })
                .catch((error) => {
                    this.setState({
                        responseError: error.data.message
                    });
                })
                .finally(() => {
                    this.setState({
                        loading: false
                    });
                })
        }
    }

    render() {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <div style={style.paper}>
                    <form onSubmit={this.handleSubmit} style={style.innerContainer} noValidate>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Link href={process.env.PUBLIC_URL + '/example.csv'}>
                                    Download example sheet
                                </Link>
                            </Grid>
                            <Grid item xs={12}>
                                <input
                                    accept="text/csv"
                                    style={{display: 'none'}}
                                    id="contained-button-file"
                                    type="file"
                                    onChange={this.handleFileChange}
                                />
                                <label htmlFor="contained-button-file">
                                    <Button variant="contained" color="primary" component="span" disabled={this.state.loading}>
                                        Select File
                                    </Button>
                                    <p>
                                        {this.state.file.name}
                                    </p>
                                </label>
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            style={style.submit}
                            disabled={this.state.loading}
                        >
                            Search
                        </Button>
                    </form>
                    <div style={style.innerContainer}>
                        {
                            this.state.loading ?
                                <LinearProgress /> :
                                ''
                        }
                        {
                            this.state.responseError ?
                                <div>{this.state.responseError}</div> :
                                ''
                        }
                    </div>
                </div>
            </Container>
        );
    }
}

export default SearchByCepList;