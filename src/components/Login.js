import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';

import api from '../api';

const style = {
    paper: {
        marginTop: '64px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: '8px',
        backgroundColor: '#35a9e1',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: '8px',
    },
    submit: {
        margin: '24px 0 16px',
    },
};

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.checkIfAlreadyLoggedIn();

        this.state = {
            login: '',
            password: '',
            loading: false,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    checkIfAlreadyLoggedIn() {
        const token = localStorage.getItem('user-token');
        if (token) {
            this.props.history.push('/')
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { login, password } = this.state;

        if (login && password) {
            return api.login({ login, password })
                .then((response) => {
                    localStorage.setItem('user-token', response.token);
                    this.props.history.push('/')
                })
                .catch((error) => {
                    alert(error.data.message);
                })
        }
    }

    render() {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div style={style.paper}>
                    <Avatar style={style.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Log in
                    </Typography>
                    <form onSubmit={this.handleSubmit} style={style.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="login"
                            label="Login"
                            name="login"
                            autoFocus
                            value={this.state.login}
                            onChange={this.handleInputChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            label="Senha"
                            type="password"
                            id="password"
                            name="password"
                            autoComplete="current-password"
                            value={this.state.password}
                            onChange={this.handleInputChange}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            style={style.submit}
                        >
                            { !this.state.loading ? 'Entrar' : <CircularProgress /> }
                        </Button>
                    </form>
                </div>
            </Container>
        );
    }
}

export default Login;