import React from "react";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import api from "../api";
import LinearProgress from "@material-ui/core/LinearProgress";
import {GoogleApiWrapper} from "google-maps-react";

import './SearchByCep.css';

const style = {
    paper: {
        marginTop: '64px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    innerContainer: {
        width: '100%', // Fix IE 11 issue.
        marginTop: '24px',
    },
    submit: {
        margin: '24px 0 16px',
    },
    result: {
        marginTop: '24px',
    }
};

const googleMapsApi = window.google;

class SearchByCep extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cep: '',
            searchResult: [],
            loading: false,
            searchError: '',
            shelters: [],
            selectedMarker: false,
            errors: {
                cep: {
                    valid: true,
                    message: ''
                }
            }
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        let value = target.value;
        value=value.replace(/\D/g,"");

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({
            errors: {
                cep: {
                    valid: true,
                    message: ''
                }
            }
        });

        const {cep} = this.state;

        if (!cep) {
            this.setState({
                errors: {
                    cep: {
                        valid: false,
                        message: 'CEP is required'
                    }
                }
            });

            return;
        }

        if (cep.length !== 8) {
            this.setState({
                errors: {
                    cep: {
                        valid: false,
                        message: 'CEP must be 8 numbers'
                    }
                }
            });

            return;
        }

        this.setState({
            loading: true,
            searchError: ''
        });

        api.searchByCep(cep)
            .then((response) => {
                const searchResultTransformed = response.results.map((pos) => {
                    pos.latitude = Number(pos.latitude)
                    pos.longitude = Number(pos.longitude)

                    return pos;
                });

                this.setState({
                    searchResult: searchResultTransformed
                });
                if (this.state.searchResult.length) {
                    this.loadMap();
                }
            })
            .catch((error) => {
                this.setState({
                    searchResult: []
                });

                this.setState({
                    searchError: error.data.message
                });
            })
            .finally(() => {
                this.setState({
                    loading: false
                });
            })
    }

    loadMap() {
        const initialPosition = {lat: this.state.searchResult[0].latitude, lng: this.state.searchResult[0].longitude};
        const map = new googleMapsApi.maps.Map(
            document.getElementById('map'), {zoom: 14, center: initialPosition});

        for (const markerInfo of this.state.searchResult) {
            const marker = new googleMapsApi.maps.Marker({
                position: { lat: markerInfo.latitude, lng: markerInfo.longitude },
                title: markerInfo.nome_fantasia,
                animation: googleMapsApi.maps.Animation.DROP,
                map: map
            });

            const infowindow = new googleMapsApi.maps.InfoWindow({
                content: `
                    <h2>${markerInfo.nome_fantasia}</h2>  
                    <p>${markerInfo.endereco}, ${markerInfo.numero}, ${markerInfo.bairro}, ${markerInfo.cidade}, ${markerInfo.estado} - ${markerInfo.cep}</p>
                    <p>${markerInfo.telefone}</p>
                `
            });

            marker.addListener("click", () => {
                infowindow.open(map, marker);
            });
        }
    }

    render() {
        return (
            <div>
                <Container component="main" maxWidth="xs">
                    <CssBaseline/>
                    <div style={style.paper}>
                        <form onSubmit={this.handleSubmit} style={style.innerContainer} noValidate>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="cep"
                                        label="CEP"
                                        name="cep"
                                        autoFocus
                                        value={this.state.cep}
                                        onChange={this.handleInputChange}
                                        error={!this.state.errors.cep.valid}
                                        helperText={this.state.errors.cep.message}
                                        inputProps={{maxlength:8}}
                                    />
                                </Grid>
                            </Grid>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                style={style.submit}
                                disabled={this.state.loading}
                            >
                                Search
                            </Button>
                        </form>
                        {
                            this.state.loading ?
                                <div style={style.innerContainer}>
                                    <LinearProgress />
                                </div>:
                                ''
                        }
                        {
                            this.state.searchError ?
                                <div style={style.innerContainer}>
                                    <div>{this.state.searchError}</div>
                                </div>:
                                ''
                        }
                    </div>
                </Container>
                {
                    this.state.searchResult && this.state.searchResult.length > 0 ?
                        <Container component="main">
                            <div style={style.innerContainer}>
                                <div id="map" style={{height: '400px', position: 'relative', width: '100%'}}/>
                            </div>
                        </Container> :
                        ''
                }
            </div>
        );
    }
}

export default GoogleApiWrapper({
        apiKey: process.env.REACT_APP_GOOGLE_MAPS_KEY,
    })(SearchByCep);