# Delivery Point Finder Fronted

### Install
- git clone
- npm install
- set the env variables
- npm start

### Pages

`/login`

`/`
Home page with the links to the features

`/search-by-cep`
Feature to search the 10 nearest delivery points from CEP

`/search-by-cep-list`
Feature to search the 10 nearest delivery points from a CEP's list